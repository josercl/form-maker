import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import Conf from './conf';
import './assets/main.css';

createApp(App)
  .use(router)
  .use(Conf)
  .mount('#app');
