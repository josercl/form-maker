import { onMounted } from 'vue';

const useChangeTitle = title => {
  onMounted(() => {
    let newTitle = 'Form Maker';
    if (title) {
      newTitle = `${newTitle} - Docs - ${title}`;
    }
    window.document.title = newTitle;
  });
};

export default useChangeTitle;
