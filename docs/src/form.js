export default [
  [
    { name: 'name', type: 'text', label: 'Text' },
    { name: 'password', type: 'password', label: 'Password' },
    {
      name: 'email', type: 'email', label: 'Email', helpText: 'Fill this field, is required',
    },
    {
      name: 'phone', type: 'tel', label: 'Phone', error: 'This field has an error',
    },
  ],
  [
    { name: 'color', type: 'color', label: 'Color' },
    {
      name: 'age', type: 'number', label: 'Number', min: 10,
    },
    { name: 'search', type: 'search', label: 'Search' },
  ],
  [
    {
      name: 'url', type: 'url', label: 'URL', placeholder: 'Hey Listen',
    },
    {
      name: 'longText', type: 'textarea', label: 'Textarea', rows: 5, class: 'textarea',
    },
    {
      name: 'select',
      type: 'select',
      label: 'Simple Select',
      options: [
        { value: 10, label: '10am' },
        { value: 11, label: '11am' },
        { value: 15, label: '3pm' },
        { value: 17, label: '5pm' },
      ],
    },
    {
      name: 'selectGrp',
      type: 'select',
      label: 'Select - Grouped Options',
      optionGroups: {
        Morning: {
          10: '10am',
          11: '11am',
        },
        Afternoon: {
          15: '3pm',
          17: '5pm',
        },
      },
    },
  ],
  [
    { name: 'birthday', type: 'date', label: 'Date' },
    { name: 'month', type: 'month', label: 'Month' },
    { name: 'time', type: 'time', label: 'Time' },
    { name: 'week', type: 'week', label: 'Week' },
  ],
  [
    {
      label: 'Range',
      name: 'range',
      type: 'range',
      min: 1,
      max: 100,
    },
    {
      label: 'Single Checkbox',
      name: 'checked',
      type: 'checkbox',
    },
    {
      label: 'Checkbox - Multiple Options',
      name: 'checkedMulti',
      type: 'checkbox',
      options: [
        { value: 10, label: '10am' },
        { value: 11, label: '11am' },
        { value: 15, label: '3pm' },
        { value: 17, label: '5pm' },
      ],
    },
    {
      label: 'Radio Button',
      name: 'radio',
      type: 'radio',
      options: [
        { value: 10, label: '10am' },
        { value: 11, label: '11am' },
        { value: 15, label: '3pm' },
        { value: 17, label: '5pm' },
      ],
    },
  ],
  [
    {
      label: 'File',
      type: 'file',
      name: 'field6',
    },
  ],
];
