import { createRouter, createWebHashHistory } from 'vue-router';
import SetupAndUsage from '../components/SetupAndUsage.vue';

const routes = [
  {
    path: '/',
    component: SetupAndUsage,
    name: 'home',
  },
  {
    path: '/theming',
    component: () => import(/* webpackChunkName: "theming" */'../components/ThemingSection.vue'),
    name: 'theming',
  },
  {
    path: '/extending',
    component: () => import(/* webpackChunkName: "extend" */'../components/ExtensionSection.vue'),
    name: 'extension',
  },
  {
    path: '/api',
    component: () => import(/* webpackChunkName: "api" */'../components/APISection.vue'),
    name: 'api',
  },
  {
    path: '/examples',
    component: () => import(/* webpackChunkName: "examples" */'../components/FormMakerExamples.vue'),
    name: 'examples',
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  linkActiveClass: 'active',
  linkExactActiveClass: 'active',
  routes,
  scrollBehavior: to => {
    if (to.hash) {
      return {
        el: to.hash,
      };
    }
    return { top: 0 };
  },
});

export default router;
