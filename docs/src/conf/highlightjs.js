import hljs from 'highlight.js';
import 'highlight.js/styles/gruvbox-dark.css';

const HighlightJS = {
  install: app => {
    const highlightDirective = (el, binding) => {
      const targets = el.querySelectorAll('code');
      targets.forEach(target => {
        if (binding.value) {
          target.textContent = binding.value;
        }
        hljs.highlightBlock(target);
      });
    };

    app.directive('highlightjs', highlightDirective);
  },
};

export default HighlightJS;
