import FormMaker from '../../../lib/index';
import HighlightJS from './highlightjs';
import FontAwesome from './fontawesome';

export default {
  install: app => {
    app.use(FormMaker)
      .use(FontAwesome)
      .use(HighlightJS);
  },
};
