import { library } from '@fortawesome/fontawesome-svg-core';
import { faNpm } from '@fortawesome/free-brands-svg-icons';
import { faHourglass } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add({ faNpm, faHourglass });

const FontAwesome = {
  install: app => {
    app.component('FontAwesomeIcon', FontAwesomeIcon);
  },
};

export default FontAwesome;
