import { resolve } from 'node:path'
import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

export default defineConfig({
  root: 'docs',
  base: process.env.NODE_ENV === 'production'
    ? '/form-maker/'
    : '/',
  plugins: [vue()],
  publicDir: 'static',
  build: {
    outDir: resolve(__dirname,'public'),
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./docs/src', import.meta.url))
    }
  }
})
