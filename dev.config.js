import vue from '@vitejs/plugin-vue';
import { fileURLToPath, URL } from 'url';

import { defineConfig } from 'vite';

export default defineConfig({
  root: 'dev',
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./dev/src', import.meta.url))
    }
  }
})
