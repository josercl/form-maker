import { createRouter, createWebHistory } from 'vue-router';
import BasicInput from '../pages/BasicInput.vue';
import SelectionInput from '../pages/SelectionInput.vue';
import DevPlayground from '../pages/DevPlayground.vue';
import SuperExample from '../pages/SuperExample.vue';

const routes = [
  {
    path: '/',
    component: BasicInput,
    name: 'text-input',
  },
  {
    path: '/selects',
    component: SelectionInput,
    name: 'select-input',
  },
  {
    path: '/playground',
    component: DevPlayground,
    name: 'playground',
  },
  {
    path: '/example',
    component: SuperExample,
    name: 'example',
  },
];

const router = createRouter({
  history: createWebHistory(),
  linkActiveClass: 'active',
  linkExactActiveClass: 'active',
  routes,
});

export default router;
