import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import FormMaker from '../../lib/index';
import './assets/main.css';
import HighlightJS from '../../docs/src/conf/highlightjs';

createApp(App)
  .use(router)
  .use(HighlightJS)
  .use(FormMaker)
  .mount('#app');
