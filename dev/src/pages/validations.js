export const required = value => !!value;

export const integer = value => !Number.isNaN(Number.parseInt(value, 10));
