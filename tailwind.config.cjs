/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './dev/**/*.{vue,html,js,ts}',
    './docs/**/*.{vue,html,js,ts}',
  ],
  theme: {
    extend: {
      colors: {
        orange: '#fc6d26',
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/forms'),
  ],
}

