import FormMakerComponent from './components/FormMaker.vue';
import FormMakerInput from './components/FormMakerInput.vue';
import CheckboxInput from './components/inputs/CheckboxInput.vue';
import FileInput from './components/inputs/FileInput.vue';
import FormMakerInputError from './components/SimpleComponent';
import FormMakerInputHelp from './components/SimpleComponent';
import FormMakerInputLabel from './components/texts/FormMakerInputLabel.vue';
import RadioInput from './components/inputs/RadioInput.vue';
import SelectInput from './components/inputs/SelectInput.vue';
import TextAreaInput from './components/inputs/TextAreaInput.vue';
import BasicInput from './components/inputs/BasicInput.vue';
import type { FormMakerPluginOptions } from './model';
import type { App, Plugin } from 'vue';

const defaultOptions: FormMakerPluginOptions = {
  classes: {
    'form-row': 'form-maker-row',
    'form-column': 'form-maker-column',
    'form-label': 'form-maker-label',

    'input-group': 'form-maker-input-group',
    'input-wrapper': 'form-maker-input-wrapper',
    'input-error': 'form-maker-input-error',
    'input': 'form-maker-input',

    'error': 'form-maker-error',
    'help-text': 'form-maker-help-text',
    'submit-button': 'form-maker-submit',
  },
  components: {
    'form-maker-input-color': BasicInput,
    'form-maker-input-date': BasicInput,
    'form-maker-input-email': BasicInput,
    'form-maker-input-month': BasicInput,
    'form-maker-input-number': BasicInput,
    'form-maker-input-password': BasicInput,
    'form-maker-input-search': BasicInput,
    'form-maker-input-tel': BasicInput,
    'form-maker-input-time': BasicInput,
    'form-maker-input-text': BasicInput,
    'form-maker-input-url': BasicInput,
    'form-maker-input-week': BasicInput,
    'form-maker-input-range': BasicInput,
    'form-maker-input-file': FileInput,
    'form-maker-input-textarea': TextAreaInput,
    'form-maker-input-select': SelectInput,
    'form-maker-input-checkbox': CheckboxInput,
    'form-maker-input-radio': RadioInput,

    'form-maker-label': FormMakerInputLabel,
    'form-maker-help': FormMakerInputHelp,
    'form-maker-error': FormMakerInputError,
  },
};

const FormMaker: Plugin = {
  install: (app: App, extraOptions: FormMakerPluginOptions = <FormMakerPluginOptions>{}) => {
    const options = {
      classes: { ...defaultOptions.classes, ...extraOptions.classes },
      components: {
        ...defaultOptions.components, ...extraOptions.components,
      },
    };

    app.component('FormMaker', FormMakerComponent);
    app.component('FormMakerInput', FormMakerInput);

    Object.keys(options.classes)
      .forEach(key => {
        app.provide(key, options.classes[key]);
      });

    Object.keys(options.components)
      .forEach(key => {
        app.component(key, options.components[key]);
      });
  },
};
export default FormMaker;
