import { computed } from 'vue';
import type {WritableComputedRef} from "vue";

type PropGetter<T> = () => T

type Emitter = (event: string, ...args: any[]) => void

export default function useVModel<T>(getter: PropGetter<T>, emit: Emitter, propName?: string): WritableComputedRef<T> {
  const fixedPropName = propName ?? 'modelValue'
  return computed({
    get: () => getter(),
    set: (value: T) => emit(`update:${fixedPropName}`, value),
  });
}
