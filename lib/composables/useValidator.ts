import type { RuleValidator } from '@/model';
import { ref, toRaw, watch, type WritableComputedRef } from 'vue';

const useValidator = (propValue: WritableComputedRef<any>, rulesArr: Array<RuleValidator> = []) => {
  const rules = toRaw(rulesArr);
  const error = ref<string | null>(null);

  watch(
    propValue,
    (newVal, oldVal) => {
      error.value = null;
      if (newVal === oldVal) return;

      for (let i = 0; i < rules.length; i += 1) {
        const rule = rules[i];
        const isValid = rule.validator(newVal);
        if (!isValid) {
          error.value = rule.message;
          break;
        }
      }
    },
    { immediate: true }
  );

  return error;
};

export default useValidator;
