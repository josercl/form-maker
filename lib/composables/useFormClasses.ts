import { inject } from 'vue';

const useFormClasses = () => {
  const labelClass = inject('labelClass', null);
  const inputClass = inject('inputClass', null);
  const inputWrapperClass = inject('inputWrapperClass', null);
  const inputGroupClass = inject('inputGroupClass', null);
  const inputErrorClass = inject('inputErrorClass', null);
  const errorClass = inject('errorClass', null);
  const helpTextClass = inject('helpTextClass', null);

  return {
    labelClass,
    inputClass,
    inputWrapperClass,
    inputGroupClass,
    inputErrorClass,
    errorClass,
    helpTextClass,
  };
};

export default useFormClasses;
