import type {Component, FunctionalComponent, InputHTMLAttributes} from "vue";

declare module '@josercl/form-maker' {
  import FormMaker from ".";
  export default FormMaker
}

export interface FormMakerPluginOptions {
  classes?: Record<string, string>,
  components?: Record<string, Component | FunctionalComponent | VNode>
}

export interface InputType {
  id?: string | null
  name: string
  type?: string | null
  label?: string | null
  columnClass?: string | null
}

export interface FormMakerProps {
  loading?: boolean
  hasActions?: boolean
  modelValue: Record<string, any>
  fields: InputType[][],
  hideDivider?: boolean
  rowClass?: string | null
  columnClass?: string | null
  labelClass?: string | null
  inputGroupClass?: string | null
  inputWrapperClass?: string | null
  inputErrorClass?: string | null
  inputClass?: string | null
  errorClass?: string | null
  helpTextClass?: string | null
  submitButtonClass?: string | null
  submitButtonText?: string | null
}

export interface FormMakerInputProps extends /* @vue-ignore */ InputHTMLAttributes{
  loading?: boolean | undefined
  disabled?: boolean | undefined
  modelValue: any,
  error?: string | undefined
  helpText?: string | undefined
  label?: string | undefined
  id?: string | undefined
  type?: string,
  rules?: RuleValidator[]
}

export interface SelectOption {
  label: string | undefined
  value: any
}

export interface RuleValidator {
  message: string
  validator: (val: any) => boolean
}
