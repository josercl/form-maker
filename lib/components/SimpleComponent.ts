import { h } from "vue"
import type { SetupContext } from "vue";

type SimpleComponentProps = {
  as?: string
}

function SimpleComponent(
  props: SimpleComponentProps,
  context: SetupContext
) {
  return h(`${props.as ?? 'div'}`, context.attrs, context.slots);
}

SimpleComponent.props = {
  as: {
    type: String,
    required: false,
    default: 'div'
  }
}

export default SimpleComponent