# Form-Maker

<img src="https://badgen.net/gitlab/license/josercl/form-maker">
<a href="https://vuejs.org/" target="_blank"><img src="https://badgen.net/badge/vue/v3?color=green"></a>
<a href="https://www.npmjs.com/package/@josercl/form-maker" target="_blank"><img src="https://badgen.net/npm/v/@josercl/form-maker/?color=red&icon=npm&label"></a>
<a href="https://www.npmjs.com/package/@josercl/form-maker" target="_blank"><img src="https://badgen.net/npm/dt/@josercl/form-maker"></a>

<p>
<img src="https://badgen.net/gitlab/release/josercl/form-maker">
<img src="https://badgen.net/gitlab/last-commit/josercl/form-maker">
</p>

## Project setup
```shell
npm install @josercl/form-maker
```

## Usage

### main.js
```javascript
import FormMaker from '@josercl/form-maker';

createApp(App)
    .use(FormMaker)
    .mount('#app')
```

### App.vue
```vuejs
<script setup>
import { reactive } from 'vue'

const person = reactive({
  name: 'John Doe',
  age: 20,
  email: 'email@example.com',
})
</script>

<template>
  <form-maker v-model="person" />
</template>
```
